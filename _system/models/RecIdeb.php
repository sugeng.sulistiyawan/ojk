<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Json;
use yii\web\NotFoundHttpException;

/**
* This is the model class for table "{{%rec_ideb_perorangan}}".
*
* @property int $id
* @property string $nomor_identitas
* @property string $nama
* @property string $tempat_lahir
* @property string $tanggal_lahir
* @property string $jenis_kelamin
* @property string $no_telepon
* @property string $nama_ibu_debitur
* @property string $alamat_sesuai_kartu
* @property string $alamat_lain
* @property string $alasan_pengajuan
* @property int $created_at
* @property int $updated_at
* @property int $is_proses
* @property integer $nomor_antrian
*/
class RecIdeb extends \app\models\master\RecIdeb
{
  public $_alamat_lain_1;
  public $_alamat_lain_2;
  public $_alamat_lain_3;
  public $_alasan_pengajuan;


  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      TimestampBehavior::className(),
    ];
  }

  /**
  * {@inheritdoc}
  */
  public function rules()
  {
    return [
      // [['nomor_identitas', 'nama', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'no_telepon', 'nama_ibu_debitur', 'alamat_sesuai_kartu', '_alamat_lain_1', 'alasan_pengajuan', 'jenis'], 'required'],
      [['nomor_identitas', 'nama', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'no_telepon', 'nama_ibu_debitur', 'alamat_sesuai_kartu', '_alamat_lain_1', 'alasan_pengajuan', 'nama_bu', 'npwp', 'nomor_akta_pendirian', 'nomor_anggaran_dasar'], 'required'],
      [['tanggal_lahir'], 'safe'],
      [['jenis_kelamin', 'alamat_sesuai_kartu', '_alamat_lain_1', 'alasan_pengajuan'], 'string'],
      // [['created_at', 'updated_at', 'is_proses'], 'integer'],
      [['nomor_identitas', 'no_telepon'], 'string', 'max' => 31],
      [['nama', 'nama_ibu_debitur'], 'string', 'max' => 63],
      [['tempat_lahir', 'nama_bu', 'npwp', 'nomor_akta_pendirian', 'nomor_anggaran_dasar'], 'string', 'max' => 127],
      [['_alamat_lain_2', '_alamat_lain_3', '_alasan_pengajuan'], 'safe'],
    ];
  }

  /**
  * {@inheritdoc}
  */
  public function attributeLabels()
  {
    return [
      'no_telepon' => 'No. Telepon',
      'alamat_sesuai_kartu' => 'Alamat Sesuai Kartu Identitas Yang Dilampirkan',
      'alamat_lain' => 'Alamat Lain Yang Pernah Ditempati',
      'alasan_pengajuan' => 'Mengajukan permohonan IDEB atas diri saya sendiri untuk kepentingan: ',
      '_alamat_lain_1' => 'Alamat Lain Yang Pernah Ditempati',
      '_alamat_lain_2' => '',
      '_alamat_lain_3' => '',
      '_alasan_pengajuan' => '',
      'is_proses' => 'Is Proses',
      'nama_bu' => Yii::t('app', 'Nama Badan Usaha'),
      'npwp' => Yii::t('app', 'NPWP'),
    ];
  }

  public function setCreate($jenis)
  {
    $this->nomor_identitas     = str_replace("-", "", $this->nomor_identitas);
    $this->nama                = ucwords(strtolower($this->nama));

    if ($jenis == 'PERSEORANGAN') {
      $this->tempat_lahir        = ucwords(strtolower($this->tempat_lahir));
      $this->tanggal_lahir       = date('Y-m-d', strtotime($this->tanggal_lahir));
      $this->jenis_kelamin       = $this->jenis_kelamin;
      $this->nama_ibu_debitur    = ucwords(strtolower($this->nama_ibu_debitur));

      $this->nama_bu              = '-';
      $this->npwp                 = '-';
      $this->nomor_akta_pendirian = '-';
      $this->nomor_anggaran_dasar = '-';
    } else {
      $this->tempat_lahir        = '-';
      $this->tanggal_lahir       = date('Y-m-d');
      $this->jenis_kelamin       = 'Laki-laki';
      $this->nama_ibu_debitur    = '-';

      $this->nama_bu              = $this->nama_bu;
      $this->npwp                 = $this->npwp;
      $this->nomor_akta_pendirian = $this->nomor_akta_pendirian;
      $this->nomor_anggaran_dasar = $this->nomor_anggaran_dasar;
    }

    $this->no_telepon          = str_replace(" ", "", $this->no_telepon);
    $this->alamat_sesuai_kartu = $this->alamat_sesuai_kartu;
    $this->alamat_lain         = $this->setAlamatLain();
    $this->alasan_pengajuan    = $this->setAlasanPengajuan();
    $this->nomor_antrian       = $this->setNomorAntrian();
    $this->jenis               = $jenis;

    return $this;
  }

  public function setAlamatLain()
  {
    $model = [
      $this->_alamat_lain_1,
      $this->_alamat_lain_2,
      $this->_alamat_lain_3,
    ];

    return Json::encode($model);
  }

  public function setAlasanPengajuan()
  {
    $model   = $this->alasan_pengajuan;
    $model[] = $this->_alasan_pengajuan;

    return Json::encode($model);
  }

  public function getAlamatLain($id)
  {
    $model  = $this->findModel($id);
    $data   = Json::decode($model->alamat_lain);

    $out = "";
    foreach ($data as $key => $value) {
      $v    = $value ? : '-';
      $k    = $key+1;
      $out .= "<tr><td class=\"td-alasan-pengajuan\" style=\"vertical-align: top;\">{$k}). &nbsp;</td><td class=\"td-alasan-pengajuan\" style=\"vertical-align: top; text-align: justify;\">{$v}</td></tr>";
    }

    return "<table class=\"list-alasan-pengajuan\"><tbody>{$out}</tbody></table>";
  }

  public function getAlasanPengajuan($id)
  {
    $model  = $this->findModel($id);
    $data   = Json::decode($model->alasan_pengajuan);

    $out = "";
    $max = count($data);
    foreach ($data as $key => $value) {
      if ($key == ($max-2)) {
        $out .= "<li>{$value}<br>{$data[$max-1]}</li>";

      } elseif ($key < ($max-1)) {
        $out .= "<li>{$value}</li>";
      }
    }

    return "<ul class=\"list-alasan-pengajuan\">{$out}</ul>";
  }

  public function formatIsProses($is_proses)
  {
    $color = "primary";
    $label = "Sudah Diproses";

    if ($is_proses == 0) {
      $color = "warning";
      $label = "Sedang Diproses";
    } elseif ($is_proses == 2) {
      $color = "danger";
      $label = "Telah Dihapus";
    }

    return "<span class=\"label label-{$color}\">{$label}</span>";
  }

  public function setNomorAntrian()
  {
    $max = static::find()
    ->where(['between', 'created_at', strtotime(date('Ymd 00:00:00')), strtotime(date('Ymd 23:59:59'))])
    ->count();

    return date('dm') . ($max + 1);
  }

  public function setTanggalIndonesia($str)
  {
    $tr   = trim($str);
    $str  = str_replace(array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'), array('Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum\'at', 'Sabtu', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'), $tr);

    return $str;
  }

  protected function findModel($id)
  {

    if (($model = static::findOne($id)) !== null) {
      return $model;
    }

    throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
  }
}
