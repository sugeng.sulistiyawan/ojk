<?php

namespace app\models\searchs;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RecIdeb as RecIdebModel;

/**
 * RecIdeb represents the model behind the search form about `app\models\master\RecIdeb`.
 */
class RecIdeb extends RecIdebModel
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['nomor_identitas', 'nama', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'no_telepon', 'nama_ibu_debitur', 'alamat_sesuai_kartu', 'alamat_lain', 'alasan_pengajuan'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $hideDeleted = false, $jenis = 'PERSEORANGAN')
    {
        $query = RecIdebModel::find()
        ->where(['jenis' => $jenis])
        ->orderBy('created_at DESC');

        if ($hideDeleted) {
          $query->andWhere(['!=', 'is_proses', 2]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'tanggal_lahir' => $this->tanggal_lahir,
        ]);

        $query->andFilterWhere(['like', 'nomor_identitas', $this->nomor_identitas])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'tempat_lahir', $this->tempat_lahir])
            ->andFilterWhere(['like', 'jenis_kelamin', $this->jenis_kelamin])
            ->andFilterWhere(['like', 'no_telepon', $this->no_telepon])
            ->andFilterWhere(['like', 'nama_ibu_debitur', $this->nama_ibu_debitur])
            ->andFilterWhere(['like', 'alamat_sesuai_kartu', $this->alamat_sesuai_kartu])
            ->andFilterWhere(['like', 'alamat_lain', $this->alamat_lain])
            ->andFilterWhere(['like', 'alasan_pengajuan', $this->alasan_pengajuan]);

        return $dataProvider;
    }
}
