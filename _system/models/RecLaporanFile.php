<?php

namespace app\models;

use Yii;

/**
* This is the model class for table "{{%rec_laporan_file}}".
*
* @property integer $id
* @property integer $id_rec_laporan
* @property string $value
*
* @property RecLaporan $idRecLaporan
*/
class RecLaporanFile extends \app\models\master\RecLaporanFile
{

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['value'], 'string', 'max' => 255],
      [['id_rec_laporan'], 'exist', 'skipOnError' => true, 'targetClass' => RecLaporan::className(), 'targetAttribute' => ['id_rec_laporan' => 'id']],
    ];
  }

}
