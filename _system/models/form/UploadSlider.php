<?php
/**
 * @link http://www.diecoding.com/
 * @author Die Coding (Sugeng Sulistiyawan) <diecoding@gmail.com>
 * @copyright Copyright (c) 2018
 */

namespace app\models\form;

use Yii;
use yii\helpers\FileHelper;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadSlider extends Model
{
  /**
  * @var UploadedFile
  */
  public $imageFile;
  public $caption;

  public function rules()
  {
    return [
      [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg', 'maxSize' => 2048000],
      [['caption'], 'required'],
    ];
  }

  public function upload()
  {
    if ($this->validate()) {
      $dir = Yii::getAlias('@webroot/images/galery') . '/';
      $img = "{$this->caption}.{$this->imageFile->extension}";

      //delete old images
      $oldImages = FileHelper::findFiles($dir, [
        'only' => [
          $img,
        ],
      ]);
      foreach ($oldImages as $i) {
        @unlink($i);
      }

      $this->imageFile->saveAs('images/galery/' . $img);
      return true;
    } else {
      return false;
    }
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'imageFile' => 'File yang didukung : png, jpg | ukuran 2 MB maksimal',
    ];
  }
}
