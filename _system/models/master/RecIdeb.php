<?php

namespace app\models\master;

use Yii;

/**
* This is the model class for table "{{%rec_ideb}}".
*
* @property integer $id
* @property string $nomor_identitas
* @property string $nama
* @property string $tempat_lahir
* @property string $tanggal_lahir
* @property string $jenis_kelamin
* @property string $no_telepon
* @property string $nama_ibu_debitur
* @property string $alamat_sesuai_kartu
* @property string $alamat_lain
* @property string $alasan_pengajuan
* @property integer $created_at
* @property integer $updated_at
* @property integer $is_proses
* @property integer $nomor_antrian
* @property string $nama_bu
* @property string $npwp
* @property string $nomor_akta_pendirian
* @property string $nomor_anggaran_dasar
* @property string $jenis
*/
class RecIdeb extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%rec_ideb}}';
  }
  
  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['nomor_identitas', 'nama', 'tempat_lahir', 'tanggal_lahir', 'jenis_kelamin', 'no_telepon', 'nama_ibu_debitur', 'alamat_sesuai_kartu', 'alamat_lain', 'alasan_pengajuan', 'nama_bu', 'npwp', 'nomor_akta_pendirian', 'nomor_anggaran_dasar', 'jenis'], 'required'],
      [['tanggal_lahir'], 'safe'],
      [['jenis_kelamin', 'alamat_sesuai_kartu', 'alamat_lain', 'alasan_pengajuan'], 'string'],
      [['created_at', 'updated_at', 'is_proses', 'nomor_antrian'], 'integer'],
      [['nomor_identitas', 'no_telepon'], 'string', 'max' => 31],
      [['nama', 'nama_ibu_debitur'], 'string', 'max' => 63],
      [['tempat_lahir', 'nama_bu', 'npwp', 'nomor_akta_pendirian', 'nomor_anggaran_dasar'], 'string', 'max' => 127],
      [['jenis'], 'string', 'max' => 255],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'nomor_identitas' => Yii::t('app', 'Nomor Identitas'),
      'nama' => Yii::t('app', 'Nama'),
      'tempat_lahir' => Yii::t('app', 'Tempat Lahir'),
      'tanggal_lahir' => Yii::t('app', 'Tanggal Lahir'),
      'jenis_kelamin' => Yii::t('app', 'Jenis Kelamin'),
      'no_telepon' => Yii::t('app', 'No Telepon'),
      'nama_ibu_debitur' => Yii::t('app', 'Nama Ibu Debitur'),
      'alamat_sesuai_kartu' => Yii::t('app', 'Alamat Sesuai Kartu'),
      'alamat_lain' => Yii::t('app', 'Alamat Lain'),
      'alasan_pengajuan' => Yii::t('app', 'Alasan Pengajuan'),
      'created_at' => Yii::t('app', 'Created At'),
      'updated_at' => Yii::t('app', 'Updated At'),
      'is_proses' => Yii::t('app', 'Is Proses'),
      'nomor_antrian' => Yii::t('app', 'Nomor Antrian'),
      'nama_bu' => Yii::t('app', 'Nama Bu'),
      'npwp' => Yii::t('app', 'Npwp'),
      'nomor_akta_pendirian' => Yii::t('app', 'Nomor Akta Pendirian'),
      'nomor_anggaran_dasar' => Yii::t('app', 'Nomor Anggaran Dasar'),
      'jenis' => Yii::t('app', 'Jenis'),
    ];
  }

}
