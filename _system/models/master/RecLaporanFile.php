<?php

namespace app\models\master;

use Yii;

/**
* This is the model class for table "{{%rec_laporan_file}}".
*
* @property integer $id
* @property integer $id_rec_laporan
* @property string $value
*
  * @property RecLaporan $idRecLaporan
  */
class RecLaporanFile extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%rec_laporan_file}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['id_rec_laporan'], 'required'],
      [['id_rec_laporan'], 'integer'],
      [['value'], 'string', 'max' => 255],
      [['id_rec_laporan'], 'exist', 'skipOnError' => true, 'targetClass' => RecLaporan::className(), 'targetAttribute' => ['id_rec_laporan' => 'id']],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'id_rec_laporan' => Yii::t('app', 'Id Rec Laporan'),
      'value' => Yii::t('app', 'Value'),
    ];
  }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getIdRecLaporan()
    {
      return $this->hasOne(RecLaporan::className(), ['id' => 'id_rec_laporan']);
    }

}
