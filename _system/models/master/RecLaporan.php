<?php

namespace app\models\master;

use Yii;
use app\models\User;

/**
* This is the model class for table "{{%rec_laporan}}".
*
* @property integer $id
* @property string $nama_laporan
* @property string $tanggal_deadline_pelaporan
* @property string $deskripsi
* @property integer $deadline
* @property string $status
* @property integer $created_at
* @property integer $updated_at
* @property integer $created_user
* @property integer $updated_user
*
  * @property User $createdUser
  * @property User $updatedUser
  * @property RecLaporanFile[] $recLaporanFiles
  */
class RecLaporan extends \yii\db\ActiveRecord
{
  /**
  * @inheritdoc
  */
  public static function tableName()
  {
    return '{{%rec_laporan}}';
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['nama_laporan', 'tanggal_deadline_pelaporan', 'deadline', 'created_at', 'updated_at', 'created_user', 'updated_user'], 'required'],
      [['tanggal_deadline_pelaporan', 'deadline'], 'safe'],
      [['deskripsi'], 'string'],
      [['created_at', 'updated_at', 'created_user', 'updated_user'], 'integer'],
      [['nama_laporan'], 'string', 'max' => 255],
      [['status'], 'string', 'max' => 31],
      [['created_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_user' => 'id']],
      [['updated_user'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_user' => 'id']],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'id' => Yii::t('app', 'ID'),
      'nama_laporan' => Yii::t('app', 'Nama Laporan'),
      'tanggal_deadline_pelaporan' => Yii::t('app', 'Tanggal Deadline'),
      'deskripsi' => Yii::t('app', 'Deskripsi'),
      'deadline' => Yii::t('app', 'Deadline'),
      'status' => Yii::t('app', 'Status'),
      'created_at' => Yii::t('app', 'Created At'),
      'updated_at' => Yii::t('app', 'Updated At'),
      'created_user' => Yii::t('app', 'Created User'),
      'updated_user' => Yii::t('app', 'Updated User'),
    ];
  }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getCreatedUser()
    {
      return $this->hasOne(User::className(), ['id' => 'created_user']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getUpdatedUser()
    {
      return $this->hasOne(User::className(), ['id' => 'updated_user']);
    }

    /**
    * @return \yii\db\ActiveQuery
    */
    public function getRecLaporanFiles()
    {
      return $this->hasMany(RecLaporanFile::className(), ['id_rec_laporan' => 'id']);
    }

}
