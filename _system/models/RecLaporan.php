<?php
/**
* @link http://www.diecoding.com/
* @author Die Coding (Sugeng Sulistiyawan) <diecoding@gmail.com>
* @copyright Copyright (c) 2018
*/


namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Json;
use yii\helpers\Html;
use yii\helpers\FileHelper;

/**
*
* This is the model class for table "{{%rec_laporan}}".
*
*
* @property integer $id
* @property string $nama_laporan
* @property string $tanggal_deadline_pelaporan
* @property string $deskripsi
* @property string $deadline
* @property integer $created_at
* @property integer $updated_at
* @property integer $created_user
* @property integer $updated_user
*
* @property User $createdUser
* @property User $updatedUser
*/
class RecLaporan extends \app\models\master\RecLaporan
{
  public $file_lampiran_pendukung;

  /**
  * {@inheritdoc}
  */
  public function behaviors()
  {
    return [
      TimestampBehavior::className(),
    ];
  }

  /**
  * @inheritdoc
  */
  public function rules()
  {
    return [
      [['nama_laporan', 'deadline', 'deskripsi'], 'required'],
      [['deadline'], 'safe'],
      [['deskripsi'], 'string'],
      [['nama_laporan', 'deskripsi'], 'trim'],
      [['nama_laporan'], 'string', 'max' => 255],
      [
        ['file_lampiran_pendukung'],
        'file',
        'skipOnEmpty' => true,
        'extensions' => 'pdf, png, jpg, jpeg, docx, doc, ppt, pptx, xls, xlsx, gif',
        'maxFiles' => 10,
      ],
    ];
  }

  /**
  * @inheritdoc
  */
  public function attributeLabels()
  {
    return [
      'tanggal_deadline_pelaporan' => Yii::t('app', 'Tanggal Pelaporan'),
      'deadline' => Yii::t('app', 'Tanggal Deadline'),
      'created_at' => Yii::t('app', 'Dibuat'),
      'updated_at' => Yii::t('app', 'Diubah'),
      'created_user' => Yii::t('app', 'Pembuat'),
      'updated_user' => Yii::t('app', 'Pengubah'),
    ];
  }

  public function getUser($id)
  {
    return User::findOne($id);
  }

  public function setCreate()
  {
    if ($this->validate()) {
      $model      = $this;

      $filename    = [];
      // $createdAt   = time();
      $namaLaporan = $this->nama_laporan;
      // foreach ($this->file_lampiran_pendukung as $i => $file) {
      //   $nama       = $namaLaporan . '_' . $createdAt . '_' . $i . '.' . $file->extension;
      //   $filename[] = $nama;
      // }
      // $deadline = date('Y-m-d', strtotime($this->tanggal_deadline_pelaporan . " + {$model->deadline} DAYS"));

      $model->id                         = (int) $model::find()->max('id') + 1;
      $model->nama_laporan               = $namaLaporan;
      $model->tanggal_deadline_pelaporan = date('Y-m-d');
      $model->deskripsi                  = $this->deskripsi;
      $model->deadline                   = $this->deadline;
      $model->created_user               = Yii::$app->user->id;
      $model->updated_user               = Yii::$app->user->id;
      // $model->created_at                 = $createdAt;
      // $model->updated_at                 = $createdAt;
      $model->status                     = 1;
      // $model->lampiran_pendukung         = Json::encode($filename);

      if ($model->save()) {

        foreach ($this->file_lampiran_pendukung as $i => $file) {
          $nama = $namaLaporan . '_' . time() . '_' . $i . '.' . $file->extension;

          $modelFile  = new RecLaporanFile();
          $modelFile->id_rec_laporan = $model->id;
          $modelFile->value          = $nama;

          if ($modelFile->save()) {

            $old = FileHelper::findFiles(Yii::getAlias('@webroot/uploads/lampiran-pendukung') . '/', [
              'only' => [
                $nama,
              ],
            ]);
            foreach ($old as $o) {
              @unlink($o);
            }

            $file->saveAs('uploads/lampiran-pendukung/' . $nama);
          }

        }

        return $model;
      }
    }

    return null;
  }

  public function setUpdate()
  {
    $this->updated_user = Yii::$app->user->id;

    return $this;
  }

  public function getLampiranPendukung($id)
  {
    $model = RecLaporanFile::find()->where(['id_rec_laporan' => $id])->all();

    if (!empty($model)) {

      $widget = \bl\files\icons\FileIconWidget::begin([
        'icons' => [
          'pdf' => [
            'icon' => Html::tag('i', '', ['class' => 'fa fa-file-pdf-o'])
          ],
          'groups' => [
            [
              'extensions' => ['jpg', 'png', 'gif', 'jpeg'],
              'icon' => Html::tag('i', '', ['class' => 'fa fa-picture-o'])
            ],
            [
              'extensions' => ['xls', 'xlsx'],
              'icon' => Html::tag('i', '', ['class' => 'fa fa-file-excel-o'])
            ],
            [
              'extensions' => ['doc', 'docx'],
              'icon' => Html::tag('i', '', ['class' => 'fa fa-file-word-o'])
            ],
            [
              'extensions' => ['ppt', 'pptx'],
              'icon' => Html::tag('i', '', ['class' => 'fa fa-file-powerpoint-o'])
            ],
          ]
        ]
      ]);

      $out = '';
      foreach ($model as $i => $m) {
        $out .= $widget->getIcon($m->value) . ' ' . Html::a($m->value, ['/uploads/lampiran-pendukung/' . $m->value], ['target' => '_blank']) . '<br>';
      }

      $widget->end();

      return $out;
    }

    return "<div class=\"label label-danger\" style=\"margin-bottom: 0;\">Tidak ada.</div>";;
  }

  public function getDeadlineVisual($id, $label = null)
  {
    $model = static::findOne($id);

    $date_start = $model->tanggal_deadline_pelaporan;
    $date_end   = $model->deadline;
    // $date_end   = date('Y-m-d', strtotime($date_start . "+ {$model->deadline} DAYS"));

    $time_start = strtotime($date_start);
    $time_end   = strtotime($date_end);

    $now   = new \DateTime(date('Y-m-d'));
    $until = new \DateTime($date_end);
    $count_end  =  $now->diff($until)->days;

    if (time() > $time_end) {
      $count_end = $count_end * -1;
    }

    // $src = Yii::$app->homeUrl . "images/deadline-img";

    if ($label == 'label') {

      if ($count_end > 0) {

        $out = "<div class=\"{$label} {$label}-warning\" style=\"margin-bottom: 0;\">
        Berakhir <strong>{$count_end}</strong> hari lagi.
        </div>";

      } elseif ($count_end == 0) {

        $out = "<div class=\"{$label} {$label}-danger\" style=\"margin-bottom: 0;\">
        Berakhir <strong>hari ini</strong>.
        </div>";

      } elseif ($count_end < 0) {

        $c   = $count_end * -1;
        $out = "<div class=\"{$label} {$label}-danger\" style=\"margin-bottom: 0;\">
        Berakhir <strong>{$c}</strong> hari yang lalu.
        </div>";

      }
    } else {

      if ($count_end > 0) {

        $endDeadline = "<h3><strong>{$count_end}</strong></h3><h5>Hari Lagi</h5>";

      } elseif ($count_end == 0) {

        $endDeadline = "<h3><strong>Hari Ini</strong></h3><h5>Terakhir</h5>";

      } elseif ($count_end < 0) {

        $count_end   = $count_end * -1;
        $endDeadline = "<h3><strong>{$count_end}</strong></h3><h5>Hari Terlewati</h5>";

      }

      $out = "<table class=\"table table-condensed\" style=\"margin: 0\">
      <tr style=\"background-color: var(--red-ojk-color);color: #fff;\">
      <td class=\"text-center\" style=\"vertical-align: middle;\">{$endDeadline}</td>
      <td style=\"vertical-align: middle;text-align: right;padding-right: 28px;\"><i class=\"glyphicon glyphicon-warning-sign\" style=\"font-size: 48px\"></i></td>
      </tr>
      </table>";

    }

    return $out;
  }

  public function formatStatus($status)
  {
    $color = "primary";
    $label = "Sudah Diproses";

    if ($status == 1) {
      $color = "warning";
      $label = "Sedang Diproses";
    } elseif ($status == 2) {
      $color = "danger";
      $label = "Telah Dihapus";
    }

    return "<span class=\"label label-{$color}\">{$label}</span>";
  }
}
