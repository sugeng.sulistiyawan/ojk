<?php
# @Author: Die Coding | www.diecoding.com
# @Date:   27 November 2017
# @Email:  diecoding@gmail.com
# @Last modified by:   Die Coding | www.diecoding.com
# @Last modified time: 17 February 2018
# @License: MIT
# @Copyright: 2017



use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

$urlParams = $generator->generateUrlParams();

echo "<?php\n";
?>

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */
?>

<div class="table-responsive">

  <?= "<?= " ?>DetailView::widget([
    'model'      => $model,
    'attributes' => [
    <?php
    if (($tableSchema = $generator->getTableSchema()) === false) {
      foreach ($generator->getColumnNames() as $name) {
        echo "     '" . $name . "',\n";
      }
    } else {
      foreach ($generator->getTableSchema()->columns as $column) {
        $format = $generator->generateColumnFormat($column);
        echo "      '" . $column->name . ($format === 'text' ? "" : ":" . $format) . "',\n";
      }
    }
    ?>
    ],
  ]
  ) ?>

</div>
