<?php
# @Author: Die Coding | www.diecoding.com
# @Date:   27 November 2017
# @Email:  diecoding@gmail.com
# @Last modified by:   Die Coding | www.diecoding.com
# @Last modified time: 17 February 2018
# @License: MIT
# @Copyright: 2017



use yii\helpers\Inflector;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $generator yii\gii\generators\crud\Generator */

echo "<?php\n";
?>

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model <?= ltrim($generator->modelClass, '\\') ?> */

?>

<?= "<?= " ?>$this->render('_form', [
  'model' => $model,
]) ?>
