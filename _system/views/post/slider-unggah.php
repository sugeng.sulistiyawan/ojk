<?php
/**
 * @link http://www.diecoding.com/
 * @author Die Coding (Sugeng Sulistiyawan) <diecoding@gmail.com>
 * @copyright Copyright (c) 2018
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\widgets\FileInput;
use yii\helpers\Url;

$this->title = 'Upload Slider';
$this->params['breadcrumbs'][] = $this->title;


?>

<div class="row">
  <div class="col-xs-12">

    <?php $form = ActiveForm::begin([
      'options' => [
        'id'  => 'form-unggah',
        'enctype' => 'multipart/form-data'
      ]
    ]
    ) ?>

    <?= $form->field($model, 'imageFile')->widget(FileInput::classname(), [
      'options' => [
        'accept' => 'image/*'
      ],
      'pluginOptions' => [
        'showCaption'     => false,
        'showUpload'      => false,
        'browseLabel'     => 'Pilih Gambar',
        'removeLabel'     => 'Hapus',
        'browseClass'     => 'btn btn-primary',
        'browseIcon'      => '<i class="glyphicon glyphicon-camera"></i> ',
      ],
    ]) ?>

    <?= $form->field($model, 'caption')
    ->textInput()
    ?>

    <?php ActiveForm::end() ?>

  </div>
</div>
