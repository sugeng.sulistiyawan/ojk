<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\tinymce\TinyMce;

/* @var $this yii\web\View */
/* @var $model app\models\Posts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="posts-form">

  <?php $form = ActiveForm::begin(['id' => 'form-post']); ?>

  <?= $form->field($model, 'judul')->textInput(['maxlength' => true]) ?>

  <?= $form->field($model, 'konten')->widget(TinyMce::className(), [
    'options' => ['rows' => 30],
    'language' => 'id',
    'clientOptions' => [
      'plugins' => [
        "advlist autolink lists link image charmap anchor",
        "visualblocks code",
        "insertdatetime media imagetools"
      ],
      'toolbar' => "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
    ]
  ]
  ) ?>

  <?php if (!Yii::$app->request->isAjax){ ?>
    <div class="form-group">
      <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Simpan') : Yii::t('app', 'Perbarui'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
  <?php } ?>

  <?php ActiveForm::end(); ?>

</div>
