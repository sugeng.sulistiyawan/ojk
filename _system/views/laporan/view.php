<?php

use yii\widgets\DetailView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RecLaporan */
?>

<div class="table-responsive">

  <?= DetailView::widget([
    'model'      => $model,
    'attributes' => [


      'nama_laporan',
      'deskripsi:ntext',
      'tanggal_deadline_pelaporan:date',
      [
        'class'     => '\kartik\grid\DataColumn',
        'label'     => 'Tanggal Deadline',
        'format'    => 'date',
        'attribute' => 'deadline',
      ],
      [
        'class'     => '\kartik\grid\DataColumn',
        'label'     => 'Status Deadline',
        'attribute' => 'deadline',
        'format'    => 'raw',
        'value' => function ($model) {

          return $model->getDeadlineVisual($model->id, 'label');
        }
      ],
      [
        'class'     => '\kartik\grid\DataColumn',
        'label'     => 'Lampiran Pendukung',
        'format'    => 'raw',
        'value'     => function($model)
        {
          return $model->getLampiranPendukung($model->id);
        }

      ],
      [
        'class'     => '\kartik\grid\DataColumn',
        'attribute' => 'created_user',
        'value'     => function($model)
        {
          return $model->getUser($model->created_user)->username;
        }
      ],
      [
        'class'     => '\kartik\grid\DataColumn',
        'attribute' => 'updated_user',
        'value'     => function($model)
        {
          return $model->getUser($model->updated_user)->username;
        }
      ],
      [
        'class'     => '\kartik\grid\DataColumn',
        'attribute' => 'created_at',
        'value'     => function($model)
        {
          return date('d-m-Y', $model->created_at);
        },
      ],
      [
        'class'     => '\kartik\grid\DataColumn',
        'attribute' => 'updated_at',
        'value'     => function($model)
        {
          return date('d-m-Y', $model->updated_at);
        },
      ],
    ],
  ]
  ) ?>

</div>
