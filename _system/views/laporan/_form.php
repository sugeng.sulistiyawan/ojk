<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use kartik\widgets\DatePicker;
use kartik\widgets\FileInput;

/* @var $this yii\web\View */
/* @var $model app\models\RecLaporan */
/* @var $form yii\widgets\ActiveForm */
?>


<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

<?= $form->field($model, 'nama_laporan')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>

<?= $form->field($model, 'deadline')->widget(DatePicker::classname(), [
  'options' => ['placeholder' => 'yyyy/mm/dd'],
  'pluginOptions' => [
    'autoclose'=>true,
    'format' => 'yyyy/mm/dd'
  ]
]); ?>

<?php
// echo $form->field($model, 'deadline', [
//   'inputTemplate' => '<div class="input-group">{input}<span class="btn input-group-addon">HARI</span></div>',
// ]
// )->textInput(['type' => 'number', 'min' => 1])->label('Deadline') ?>

<?= $form->field($model, 'file_lampiran_pendukung[]')->widget(FileInput::classname(), [
  'attribute' => 'file_lampiran_pendukung[]',
  'options' => ['multiple' => true],
  'pluginOptions' => [
    'showUpload' => false,
  ],
]
) ?>

<?php if (!Yii::$app->request->isAjax){ ?>
  <div class="form-group">
    <?= Html::submitButton($model->isNewRecord ? Yii::t('app', 'Simpan') : Yii::t('app', 'Perbarui'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
  </div>
<?php } ?>

<?php ActiveForm::end(); ?>
