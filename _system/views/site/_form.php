<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\MaskedInput;
use kartik\widgets\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\master\RecIdeb */
/* @var $form yii\widgets\ActiveForm */


$js = <<< JS
$("#recideb-_alasan_pengajuan").hide();

$("[name='RecIdeb[alasan_pengajuan][]']").click(function() {
  if ($("[name='RecIdeb[alasan_pengajuan][]']")[2]['checked']) {
    $("#recideb-_alasan_pengajuan").show();
  } else {
    $("#recideb-_alasan_pengajuan").hide();
  }
});
JS;

$this->registerJs($js);

?>

<div class="row">

  <?php $form = ActiveForm::begin(); ?>

  <div class="col-md-6">

    <?= $form->field($model, 'nomor_identitas')->widget(MaskedInput::className(), [
      'mask' => '9999-9999-9999-9999',
    ]
    ) ?>

    <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

    <?php if ($p) { ?>

      <?= $form->field($model, 'tempat_lahir')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'tanggal_lahir')->widget(DatePicker::className(), [
          'pluginOptions' => [
            'autoclose'=>true,
            'format' => 'dd-mm-yyyy'
        ]
      ]
      ) ?>

      <?= $form->field($model, 'jenis_kelamin')->dropDownList([ 'Laki-laki' => 'Laki-laki', 'Perempuan' => 'Perempuan', '' => '', ], ['prompt' => '']) ?>

      <?= $form->field($model, 'no_telepon')->widget(MaskedInput::className(), [
        'mask' => ['+62 999 999999', '+62 999 9999999', '+62 999 99999999', '+62 999 999999999'],
      ]
      ) ?>

      <?= $form->field($model, 'nama_bu')->hiddenInput(['value' => '-'])->label(false) ?>

      <?= $form->field($model, 'npwp')->hiddenInput(['value' => '-'])->label(false) ?>

      <?= $form->field($model, 'nomor_akta_pendirian')->hiddenInput(['value' => '-'])->label(false) ?>

      <?= $form->field($model, 'nomor_anggaran_dasar')->hiddenInput(['value' => '-'])->label(false) ?>

    <?php } else { ?>

      <?= $form->field($model, 'nama_bu')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'npwp')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'alamat_sesuai_kartu')->textarea(['rows' => 3])->label('Alamat Sesuai NPWP') ?>

      <?= $form->field($model, 'tempat_lahir')->hiddenInput(['value' => '-'])->label(false) ?>

      <?= $form->field($model, 'tanggal_lahir')->hiddenInput(['value' => '-'])->label(false) ?>

      <?= $form->field($model, 'jenis_kelamin')->hiddenInput(['value' => '-'])->label(false) ?>

      <?= $form->field($model, 'nama_ibu_debitur')->hiddenInput(['value' => '-'])->label(false) ?>

    <?php } ?>

  </div>

  <div class="col-md-6">

    <?php if ($p) { ?>

      <?= $form->field($model, 'nama_ibu_debitur')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'alamat_sesuai_kartu')->textarea(['rows' => 3]) ?>

    <?php } else { ?>

      <?= $form->field($model, 'no_telepon')->widget(MaskedInput::className(), [
        'mask' => ['+62 999 999999', '+62 999 9999999', '+62 999 99999999', '+62 999 999999999'],
      ]
      ) ?>

      <?= $form->field($model, 'nomor_akta_pendirian')->textInput(['maxlength' => true]) ?>

      <?= $form->field($model, 'nomor_anggaran_dasar')->textInput(['maxlength' => true]) ?>

    <?php } ?>

    <?= $form->field($model, '_alamat_lain_1')->textarea(['rows' => 1, 'placeholder' => '1) ...']) ?>
    <?= $form->field($model, '_alamat_lain_2')->textarea(['rows' => 1, 'placeholder' => '2) ...'])->label(false) ?>
    <?= $form->field($model, '_alamat_lain_3')->textarea(['rows' => 1, 'placeholder' => '3) ...'])->label(false) ?>

  </div>

  <div class="col-md-12" style="margin-top: 40px;">

    <?php if ($p) { ?>

      <?= $form->field($model, 'alasan_pengajuan')->checkboxList([
        'Dokumentasi diri sendiri'                           => 'Dokumentasi diri sendiri',
        'Dokumentasi untuk pengajuan kredit atau pembiayaan' => 'Dokumentasi untuk pengajuan kredit atau pembiayaan',
        'Lain-lain: '                                        => 'Lain-lain: ',
      ]
      )
      ->inline(true) ?>

    <?php } else { ?>

      <?= $form->field($model, 'alasan_pengajuan')->checkboxList([
        'Dokumentasi BU sendiri'                             => 'Dokumentasi BU sendiri',
        'Dokumentasi untuk pengajuan kredit atau pembiayaan' => 'Dokumentasi untuk pengajuan kredit atau pembiayaan',
        'Lain-lain: '                                        => 'Lain-lain: ',
      ]
      )
      ->inline(true) ?>

    <?php } ?>

    <?= $form->field($model, '_alasan_pengajuan')->textarea(['rows' => 1, 'placeholder' => '...'])->label(false) ?>

  </div>


  <?php if (!Yii::$app->request->isAjax){ ?>
    <div class="form-group">
      <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Perbarui', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
  <?php } ?>

  <?php ActiveForm::end(); ?>

</div>
