<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\bootstrap\Modal;
use kartik\grid\GridView;
use johnitvn\ajaxcrud\CrudAsset;

CrudAsset::register($this);

$this->params['btn-menu'][] = "
<div class=\"btn-group\">
  <button type=\"button\" class=\"btn btn-primary dropdown-toggle\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
    Isi Formulir <span class=\"caret\"></span>
  </button>
  <ul class=\"dropdown-menu\" style=\"right: 0; left: auto;\">
  <li>" . Html::a('Permohonan IDEB Perseorangan', ['create', 'action' => 'perseorangan'], [
    'role'  => 'modal-remote',
    'title' => 'Isi Formulir Permohonan IDEB Perseorangan',
  ]) .
  "</li>
  <li>" . Html::a('Permohonan IDEB Badan Usaha', ['create', 'action' => 'badan-usaha'], [
    'role'  => 'modal-remote',
    'title' => 'Isi Formulir Permohonan IDEB Badan Usaha',
  ]) .
  "</li>
</ul></div>";

$js = <<< JS

setInterval(function(){
  $('.kv-loader-overlay').hide();
  $.pjax.reload({container: '#crud-datatable-pjax'});
}, 10000);

JS;

$this->registerJs($js);

?>

<div class="row">
  <div class="col-md-4">

    <?= $this->render('slider') ?>

  </div>

  <div class="col-md-8">

    <div id="ajaxCrudDatatable">
      <?= GridView::widget([
        'id'             =>'crud-datatable',
        'dataProvider'   => $dataProvider,
        // 'filterModel'    => $searchModel,
        'pjax'           => true,
        'columns'        => require(__DIR__.'/_columns.php'),
        'striped'        => true,
        'condensed'      => true,
        'responsive'     => true,
        'responsiveWrap' => false,
        // 'toolbar'      => [
        //   [
        //     'content' => '{toggleData}'
        //   ],
        // ],
        'panel' => [
          'type'    => 'default',
          'heading' => '{toggleData} - Timeline Pelaporan',
          'before'  => false,
          'after'   => false,
          'footer'  => false,
        ]
        ]) ?>
      </div>

      <?php

      Modal::begin([
        "id"     => "ajaxCrudModal",
        "size"   => "modal-lg",
        "footer" => "", // always need it for jquery plugin
      ]);

      Modal::end();

      ?>
    </div>
  </div>
